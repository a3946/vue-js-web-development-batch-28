var hobbies = ["coding", "cycling","climbing","skateboard"]
// console.log(hobbies)
// console.log(hobbies.length);
// console.log(hobbies[0]) 
// console.log(hobbies[hobbies.length - 1]); 

//Metode array

var feeling = ["dag","dig"]
feeling.push("dug") //menambahkan nilai "dug" ke index paling belakang
feeling.pop();


// PUSH => Push adalah metode array untuk menambahkan nilai di belakang elemen terakhir di array. metode push menerima sebuah parameter yaitu nilai yang ingin kita tambahkan ke dalam array.

var numbers = [0,1,2]
numbers.push(3)
// console.log(numbers)
// bisa juga memasukan lebih dari satu nilai menggunkan metode push
numbers.push(4, 5)
// console.log(numbers)

//POP => Pop adalah kebalikan dari push yaitu menghapus nilai elemen paling terakhir dari sebuah array. metode Pop tidak menerima parameter apapun sehingga metode pop hanya bisa mengeluarkan satu elemen saja yaitu yang paling terakhir dari sebuah array
 var number = [0, 1, 2, 3, 4, 5]
 numbers.pop()
//  console.log(numbers)

// UNSHIFT => yaitu menambahkan nilai pada index ke-0 sehingga elemen-elemen sebelumannya bergeser 

var numbers = [0, 1, 2, 3]
numbers.unshift(22)
// console.log(numbers);

//SHIFT => kebalikan dari unshift yaitu menghapus nilai pada elemen terdepan sebuah array. metode shift tidak menerima parameter apapun.

var numbers = [0, 1, 2, 3]
numbers.shift(0)
// console.log(numbers);

//SORT => metode untuk mengurutkan nilai pada array. secara otomatis, sort akan mengurutkan secara ascending(dari rendah ke tinggi) dan diurutkan berdasarakan unicode dari karakter. urutan unicode artinya secara lebih besar dibandingkan dengan karakter yang lainnya. contohnya adalah karakter "b" akan lebih besar dari pada "a" karakter "c" lebih besar dari pada karakter "b" dst.
var animals = ["kera", "gajah", "musang"]
animals.sort()
// console.log(animals);

//Slice => metode untuk mengambil irisan dari sebuah array. metode slice menerima satu atau dua parameter. parameter pertama adalah nomor index pertama yang kita ambil sebagai irisan, sedangkan parameter kedua adalah parameter kedua adalah index terakhir yang ingin kita ambil sebagai irisan.

var angka = [0, 1, 2, 3, 4]
var irisan1 = angka.slice(1, 3) // 1, 4
// console.log(irisan1)
var irisan2 = angka.slice(2, 4) // 2,
// console.log(irisan2)
var irisan3 = angka.slice(2) 
// console.log(irisan3)

var angka = [0, 1, 2, 3]
var salinAngka = angka.slice()
// console.log(salinAngka)


//Splice =>  metode untuk menghapus dan/atau menambahkan nilai elemen pada array. Metode splice bisa menerima parameter sebanyak dua atau lebih parameter. Jika ingin menggunakan splice untuk menghapus elemen pada index tertentu maka digunakan 2 paramater. Jika ingin menggunakan splice untuk menambahkan elemen pada index tertentu maka digunakan tiga parameter.

var fruits = ["banana","orange","grape"]
fruits.splice(1, 0, "watermelon")
// fruits.splice(0, 2)
// fruits.splice(0, 2)
// console.log(fruits);


//SPLIT AND JOIN metode split yaitu memecah sebuah string sehingga menjadi sebuah array. Split menerima sebuah parameter berupa karakter yang menjadi seperator untuk memecah string.

var biodata = "name: John,Doe"
var name = biodata.split(":")
// console.log(name)

var title = ["my", "first", "experience", "as", "progammer"]
var slug = title.join("-")
// console.log(slug);


//FUNCTION => sebuah blok kode yang disusun sedemikian rupa untuk menjalankan sebuah tindakan. blok kode ini dibuat untuk dapat bisa digunakan kembali. cara atau bentuk penulisan function adalah sebagai berikut

// function nama_function(parameter 1, parameter 2, ...) {
//     [isi dari function berupa tindaka]
//     return [Expression];
// }

// Kode di atas tidak dapat kita copy-paste kan langsung, melainkan hanya sebuah bentuk penulisan function. sebuah function, umumnya melalukakn tindakan dan sebelum function berakhir, function bisa mengembalikan nilai dengan cara sintaks return.

function tampilkan() {
    // console.log("halo!")
}

tampilkan();

function munculkanAngkaDua() {
    return 2 
}

var tampung = munculkanAngkaDua();
// console.log(tampung);

//Function dengan parameter

function kalikanDua(angka) {
    return angka * 2
}

var tampung = kalikanDua(2);
// console.log(tampung);

//Pengiriman parameter lebih dari satu

function tampilkanAngka(angkaPertama, angkaKedua) {
    return angkaPertama + angkaKedua
}

// console.log(tampilkanAngka(5, 4))

// Inisialisasi parameter dengan nilai default

function tampilanAngka(angka = 1) {
    return angka
}

//console.log(tampilanAngka(5))// 5, sesuai dengan parameter yang dikirim
//console.log(tampilanAngka())// 1 ,karena default parramtet adalah 1

var fungsiPerkalian = function(angkaPertama, angkaKedua) {
    return angkaPertama * angkaKedua
}

// console.log(fungsiPerkalian(2, 4))

//OBJECT
var personaArr = ["John","Doe","male",25]
var personObj = {
    firstName : "John",
    lastName : "Doe",
    Age : 27
}

console.log(personaArr[0])
console.log(personObj.lastName)

//Deklararis Objek

// var oject = {
//     key : value
// }

var car = {
    brand : "Ferarri",
    type : "Sport Car",
    price : 5000000,
    "horserpower" : 720
}

var motorcycle1 = {
    brand : "Honda",
    type : "CUB",
    price : 1000
}

console.log(motorcycle1.brand) // "Honda"
console.log(motorcycle1.type)


var mobil = [{merk : "BMW", warna : "merah", tipe: "sedan"},
            {merk : "toyota", warna : "hitam", tipe: "box"},
            {merk : "Hyundai", warna : "putih", tipe: "listrik"}]


//ARRAY Iteration

//array iteration merupakan dalam array untuk melakukan perulanagan data dari array, method array iteration ada banyak tapi untuk basic kita hanya perlu menggunakan 3 method ini yaitu forEach(), map() dan filter()

mobil.forEach(function(item){
    console.log("warna :" + item.warna)
})

//map

var arrayWarna = mobil.map(function(item){
    return item.warna
})

console.log(arrayWarna);

var arrayMobilFilter = mobil.filter(function(item){
    return item.tipe != "sedan";
})

console.log(arrayMobilFilter);

