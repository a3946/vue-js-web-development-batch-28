if (true) {
    // console.log("jalankan code");
}

if (false) {
    // console.log("code tidak di jalankan");
}

var mood = "happy" 
if ( mood == "happy") {
    // console.log("hari ini aku bahagia");
}

var minimarketStatus = "Open"
if (minimarketStatus == "open") {
    // console.log("saya akan membeli telur dan buah")
} else {
    // console.log ("minimarketnya tutup");
}

var minimarketStatus = "close"
var minuteRemainingToOpen = 5
if (minimarketStatus == "open"){
//     console.log("saya akan membeli telur dan buah")
// } else if (minuteRemainingToOpen <= 5) {
//     console.log("minimarket buka sebentar lagi, saya tungguin")
// } else {
//     console.log ("minimarketnya tutup, saya pulang lagi");
}

//Contoh 6 conditional bersarang 
var minimarketStatus = "open"
var telur = "soldout"
var buah = "ada"

// if (minimarketStatus == "open") {
//     console.log("saya akan membeli telur dan buah")
//     if(telur == "soldout" || buah == "soldout") {
//         console.log("belanjaan saya tidak lengkap")
//     } else if (telur == "soldout") {
//         console.log("telur habis")
//     } else if (buah == "soldout") {
//         console.log("buah habis")
//     }
// } else  {
//     console.log ("minimarket tutup, gue pulang aja")
// }

//Switch Case
// var buttonPushed = 3; 
// switch(buttonPushed) {
//     case 1 : {console.log('matikan tv');break;}
//     case 2 : {console.log('turunkan volume tv');break;}
//     case 3 : {console.log('tingkatkan volume tv');break}
//     case 4 : {console.log('matikan suara TV!');break}
//     default : {console.log('tidak terjadi apa - apa');}
// }

//Looping
var flag = 1;
while (flag < 10) {
    // console.log('Iterasi ke ' + flag);
    flag++;
}

//Contoh Looping While-loop 2
var deret = 5
var jumlah = 0;
while(deret > 0) {
    jumlah += deret;
    deret--;
    // console.log('Jumlah saat ini' +jumlah);
}

//For loop
for (var angka = 1; angka < 10; angka++) {
    console.log('iterasi ke ' + angka);
}


//looping 2 
var jumlah = 0;
for (var deret = 5; deret > 0; deret--){
    jumlah +=deret;
    console.log('Jumlah saat ini ' + jumlah)
}
    console.log('Jumlah Terakhir ' + jumlah)

//looping 3
var flag = 1 
while (flag < 10) {
    console.log('iterasi ke ' + flag);
}