
// ES6

let x = 1;
if (x === 1) {
    let x = 2;

    // console.log(x)
}

// console.log(x)

const number = 42;
// number = 100;

function multiply(a, b = 8) {
    return a * b
}

// console.log(multiply(5, 2))
// console.log(multiply(5));
// console.log(multiply());

// template literals 

// const firstName = 'John';
// const lastName = 'Doe';
// const teamName = 'Mr';

// const theString = `${firstName} ${lastName}, ${teamName}`

// console.log(theString)

// // Before ES6 Javascript:

// const fullName = 'John Doe'
 
// const john = {
//   fullName: fullName
// }

//ES 6 Javascript : 

// const fullName = 'John Doe'
// const john = {fullName}

// Destructuring

// Destructuring merupakan eksepresi javascript yang memungkinkan untuk membagi atau memecah nilai dari sebuah array atau objek ke dalam variabel yang bebeda

//array

// var numbers = [1,2,3]

// var numberOne = numbers[0]
// var numberTwo = numbers[1]
// var numberThree = numbers[2]

// console.log(numberOne)

//object 
// var studentName = {
//     firstName : 'Peter',
//     lastName : 'Parker'
// }

// const firstName = studentName.firstName;
// const lastName = studentName.lastName;

// console.log(firstName)


//dengan destructuring

//array

let numbers = [1,2,3]

const [numberOne, numberTwo, numberThree] = numbers

// console.log(numberOne)

//object
var studentName = {
    firstName: 'Peter',
    lastName: 'Parker'
}

const {firstName, lastName} = studentName

// console.log(firstName)

//Rest Parameters + Spread Operator

// Rest Parameter

//First Example

let scores = ['98','95','93','90','87','85','69']
let [first, second, third, ...restOfScores] = scores;

// console.log(first)
// console.log(second)
// console.log(third)
// console.log(restOfScores);

//second example 
const filter = (...rest) => {
    return rest.filter(el => el.text !== undefined)
}

// console.log(filter, {text: "wonderful"}, "next")

//spread operator
let array1 = ['one','two']
let array2 = ['three','four']
let array3 = ['five', 'six']

//ES 5 WAY
// var combinedArray = array1.concat(array2).concat(array3)
// console.log(combinedArray)


//ES 6 WAY

let combinedArray = [...array1,...array2,...array3 ]
console.log(combinedArray) 

//Asynchronous

